package com.unifun.voice.jsonmapper.model;

import lombok.Data;

@Data
public class LogoutResponse {
    private String user;
    private String jwt;
    private String refreshToken;
}

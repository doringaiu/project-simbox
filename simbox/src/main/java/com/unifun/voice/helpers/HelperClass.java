package com.unifun.voice.helpers;

import lombok.NoArgsConstructor;

import java.util.Base64;
import java.util.UUID;

@NoArgsConstructor
public class HelperClass {

    public String genUUID() {
        String resultUUID = UUID.randomUUID().toString();
        resultUUID += UUID.randomUUID().toString();
        resultUUID += UUID.randomUUID().toString();
        System.out.println(resultUUID);
        return resultUUID;
    }

    public String decodeBase64(String request) {
        try {
            byte[] requestBytes = request.getBytes("UTF-8");
            byte[] decodedRequest = Base64.getDecoder().decode(requestBytes);
            String result = new String(decodedRequest);
            return result;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
}

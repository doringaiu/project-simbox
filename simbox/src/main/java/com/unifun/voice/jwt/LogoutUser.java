package com.unifun.voice.jwt;


import com.unifun.voice.jsonmapper.model.LogoutResponse;
import com.unifun.voice.orm.repository.SessionTableRepository;

import javax.enterprise.context.RequestScoped;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@RequestScoped
@Path("/logout")
public class LogoutUser {
    @POST
    public Response logoutUser(String req) {
        Jsonb jsonb = JsonbBuilder.create();
        LogoutResponse response = jsonb.fromJson(req, LogoutResponse.class);
        System.out.println(response.getJwt() + "     " + response.getUser());
        String token = response.getRefreshToken(); // req.body refresh token
        if(LoginUser.getRefreshTokenInstance().containsValue(token)) {
            LoginUser.getRefreshTokenInstance().entrySet()
                    .removeIf(
                            entry -> (token
                                    .equals(entry.getValue())));

        }

        // remove session
        SessionTableRepository str = new SessionTableRepository();
        //str.removeSessionByToken(response.getJwt());

        //
        return Response.status(Response.Status.NO_CONTENT).build();
    }
}

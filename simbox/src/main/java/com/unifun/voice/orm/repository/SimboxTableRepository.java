package com.unifun.voice.orm.repository;

import com.unifun.voice.orm.model.SimboxTable;
import lombok.NoArgsConstructor;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
@NoArgsConstructor

public class SimboxTableRepository {

    @Inject
    EntityManager entityManager;

    @Transactional
    public void addSimbox(SimboxTable simboxTable) {
        entityManager.persist(simboxTable);
    }

    @Transactional
    public void removeSimbox(SimboxTable simboxTable) {
        entityManager.remove(simboxTable);
    }

    @Transactional
    public List<SimboxTable> getSimboxes() throws Exception {
        Query query = entityManager.createQuery(" SELECT s from SimboxTable as s");
        return query.getResultList();
    }
}

package com.unifun.voice.orm.repository;

import com.unifun.voice.orm.model.SessionTable;
import lombok.NoArgsConstructor;


import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
@NoArgsConstructor
public class SessionTableRepository {
    @Inject
    EntityManager entityManager;

    @Transactional
    public void addSession(SessionTable sessionTable) {
        entityManager.persist(sessionTable);
    }

    @Transactional
    public void removeSession(SessionTable sessionTable) {
        sessionTable = fillMissingID(sessionTable);
        try {
            entityManager.remove(entityManager.contains(sessionTable) ? sessionTable : entityManager.merge(sessionTable));
        } catch(java.lang.IllegalArgumentException e) {
            System.out.println("Cannot delete an entity that was already deleted");
        }
    }

    @Transactional
    public List<SessionTable> getSessions() throws Exception {
        Query query = entityManager.createQuery("SELECT s from SessionTable as s");
        List<SessionTable> st =  query.getResultList();
        return st;
    }

    private SessionTable fillMissingID(SessionTable st) {
        try {
            List<SessionTable> stl = getSessions();
            for(SessionTable tmp: stl) {
                if(tmp.getToken().equals(st.getToken())) {
                    st.setId(tmp.getId());
                    return st;
                }
            }
        } catch(Exception e) {
            System.out.println(e);
            return null;
        }
        return null;
    }

    @Transactional
    public List<String> getTokens() throws Exception {
        Query query = entityManager.createQuery("SELECT token FROM SessionTable");
        return query.getResultList();
    }

    void printAllIDs(List<SessionTable> st) {
        for(SessionTable s: st) {
            System.out.println("id: " + s.getId());
        }
    }

    public SessionTable getNewSession(String ipAddr, String token) {
        SessionTable st = new SessionTable();
        st.setIpAddr(ipAddr);
        st.setToken(token);
        //id is auto generated
        return st;
    }

    public boolean checkIfTokenIsNotInDB(String token) {
        try {
            for (String temp : getTokens()) {
                if(token.equals(temp)) {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }

    //    @Transactional
//    public String getIPByToken(String token) {
//        System.out.println("getting ip by token");
//        Query query = entityManager.createQuery("SELECT ipaddr FROM SessionTable WHERE token=" + token);
//        return query.getSingleResult().toString();
//    }

//    @Transactional
//    public void removeSessionByToken(String token) {
//        System.out.println("Removing session by token");
//        try {
//            for (String tkn : getTokens()) {
//                if(tkn.equals(token)) {
//                    removeSession(getNewSession(getIPByToken(token), token));
//                }
//            }
//        } catch(Exception e) {
//
//        }
//    }

//    public void checkForExpiredTokensAndRemoveThem() {
//        TokenManager tokenManager = new TokenManager();
//        try {
//            for(String token: getTokens()) {
//                if(tokenManager.verifyIfTokenIsValid(token, Constants.SECRET_KEY)) {
//
//                }
//            }
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//    }
}

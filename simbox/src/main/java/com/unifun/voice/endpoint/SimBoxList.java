package com.unifun.voice.endpoint;

import com.unifun.voice.orm.repository.SimboxTableRepository;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@RequestScoped
@Path("/simboxlist")
public class SimBoxList {
    private Jsonb jsonb = JsonbBuilder.create();
    @Inject
    SimboxTableRepository simboxTableRepository;
    @GET
    public String doGet() {
        try {
            return jsonb.toJson(simboxTableRepository.getSimboxes());
        } catch(Exception e) {
            return e.toString();
        }
    }
}